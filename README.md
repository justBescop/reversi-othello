# Reversi/Othello

Le projet a pour but d'implémenter le jeu du Reversi/Othello avec un coté console et un autre coté gui qui est aussi implémenté en ligne.

## Controles

Console :
Entrer un numéro d'action valide pour jouer

Interface graphique :
 - Clic gauche sur une case : jouer un jeton
 - Clic milieu de la souris : recommencer une partie
 - Clic droit de la souris : quitter le jeu
 
## Description du modèle

Diagramme de classe de Reversi 
![classdiagram](reversiClassDiagram.png) 

Etat transition
![classdiagram](reversiEtatTransition.png)

Diagramme de classe avec la Gui 
![classdiagram](diagramclassbegining.png)

Diagramme de classe final
![classdiagram](reversidiagramF.png)
 

Implémentation C++/JavaScript du jeu de Reversi/Othello.

- [site en ligne](https://justbescop.gitlab.io/reversi-othello/) Documentation/Jeu

- [page de cours](https://juliendehos.gitlab.io/posts/projet-cpp-gl/post86-2019-2020.html)


## Compilation du projet C++ -- En local

Projet CMake classique, dans le dossier `cpp`.

### Execution des fichiers C++
```
./reversi-cli
./reversi-test
./reversi-gui
```
## Compilation du projet JavaScript

Aller dans le dossier 'js' puis :

```
make
```
et à la fin de la compilation :

```
python -m http.server --directory . -b 127.0.0.1
```
enfin ouvrir un navigateur et aller sur l'adresse 'localhost:8000/'.

## Bilan de projet

J'ai pu avancer dans le projet jusqu'à la fin du jalon 2. Ainsi j'ai pu implémenter le jeu Reversi/Othello via une interface console lié avec les tests unitaires, une interface en utilisant la librairie SDL en effectuant les tests par vérification manuelle.
Enfin j'ai hébergé le jeu sur la page gitlab du projet en utilisant emscripten.
Malheureusement je n'ai pas eu le temps de commencer le jalon 3 à savoir le développement d'IA.

### Bugs référencés
Lors d'un passage de tour sur la gui hébergée sur le web, l'affichage ne change pas le joueur mais reviens quand même à la normale après un coup.

### Commentaires sur l'implémentation et le modèle choisi
Mon modèle est divisé en 2 classes et plusieurs fichiers cpp mains.
Il y a une classe Reversi qui implémente toutes les fonctionnalités du jeu et la classe Gui qui implémente l'interface graphique sous SDL.
Les librairies à utiliser étaient prédéfinies, le modèle devait donc répondre à une implémentation d'une interface SLD2 sous Webassembly via emscripten.
L'instance du jeu n'est pas lancé avec la classe jeu mais il faut une boucle extérieure, soit dans un main (comme dans reversi-cli.cpp) soit via une autre classe (comme les méthodes Gui::instance() et Gui::iteration())

Les points discutables de mon modèle sont :
 - La gestion de la fin de partie. En effet j'attend que les 2 joueurs ne puissent pas jouer pour déclarer la fin de partie. Cette implémentation est génante dans une gui car lorsqu'un tour s'effectue via gestion du clic il faut passer les tours des joueurs en cliquant dans le vide.
 - Aussi je pourrais rajouter un indicateur pour les coups possibles, ce qui améliorerai l'expérience utilisateur

### Problèmes rencontrés lors du projet
Le principal problème qui m'a vraiment fait perdre du temps c'est l'assimilation des librairies. Il a fallu déjà lire la documentation ainsi que le projet test pour comprendre l'imbrication des fichiers. Je n'ai pas tout compris au début mais au fur et à mesure de corriger les erreurs j'ai réussi à comprendre la manière dont ça fonctionnait.
Cette méthode m'a fait perdre beaucoup de temps et je n'ai donc pas pu finir le projet à 100%. Aussi étant donné que je n'avais pas compris le fonctionnement de emscripten ou de la SDL2 j'ai eu du mal à imaginer un diagramme correct pour préparer le jalon.
Les points sur lesquels je me suis attardés sont :
 - La compilation du projet avec la SDL2 avec le docker
 - L'hébergement du projet sur le web avec un problème pour les boucles bloquantes ainsi que du chargement de la police sur gitlab
 - Bien sur le fait de retourner des jetons et d'anticiper les actions possibles m'a pris du temps à implémenter mais je n'ai pas eu de difficulté pour le faire
 
### Conclusion 
Il est sûr que pour d'autres projets je vais devoir gérer mon temps d'une meilleure façon encore plus quand je devrai utiliser des librairies que je ne connaîs pas
