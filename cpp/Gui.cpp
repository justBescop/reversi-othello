#include "Gui.hpp"

#include <iostream>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <SDL2/SDL_ttf.h>
#include <string>

const char * WINDOW_TITLE = "Reversi/Othello"; //Titre de notre fenêtre
const int WINDOW_WIDTH = 700;// Largeur de la fenêtre
const int WINDOW_HEIGHT = 400;//Hauteur de la fenêtre


Gui::Gui(){
	
}

void Gui::guiInit(){
	//INIT 
	if (SDL_Init(SDL_INIT_VIDEO) <0){
		std::cerr << "init error: " << SDL_GetError() << std::endl;
		exit(-1);
	}
	// Créé la fenetre avec sa taille et son titre
	window = SDL_CreateWindow(WINDOW_TITLE, SDL_WINDOWPOS_UNDEFINED, 
							SDL_WINDOWPOS_UNDEFINED, WINDOW_WIDTH,
							WINDOW_HEIGHT, SDL_WINDOW_SHOWN);
							
	if(window == nullptr) {
		std::cerr << "window error: " << SDL_GetError() << std::endl;
		exit(-1);
	}
	
	//Créé le renderer
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	if (renderer==nullptr) {
		std::cerr << "renderer error : " << SDL_GetError() << std::endl;
		exit(-1);
	}
	
	//INIT DU TTF
	if(TTF_Init() ==-1) {
		exit(-1);
	}
	police = TTF_OpenFont("LiberationMonoBold.ttf",12);
	if (police==nullptr) {
		std::cerr << "police error : " << TTF_GetError() << std::endl;
		exit(-1);
	}
}

void Gui::guiRender(){
	
	//Rendu du fond de couleur verte
	SDL_SetRenderDrawColor(renderer,0,255,0,255);
	SDL_RenderClear(renderer);
	
	//Rendu des lignes / quadrillage
	SDL_SetRenderDrawColor(renderer,0,0,0,255);
	for(int i=50;i<400;i+=50)
	{
		SDL_RenderDrawLine(renderer,i,0,i,400);
		SDL_RenderDrawLine(renderer,0,i,400,i);
	}
	
	//Rendu des jetons
	int compteurX =0;
	int compteurY =0;
	int x;
	int y;
	for (auto & l : jeu.getPlateau()) // on parcours le plateau
    {
		compteurY=0;
		for (auto & c : l)
		{
			x=compteurX*50+25;
			y=compteurY*50+25;
			if(c == JOUEUR_NOIR){filledCircleRGBA(renderer,x,y,20,0,0,0,255);} //cercle noir
			else if (c == JOUEUR_BLANC){filledCircleRGBA(renderer,x,y,20,255,255,255,255);}//cercle blanc
			compteurY++;
		}
		compteurX++;
	}
		
	//--------------//
	//Affichage des textes
	
	SDL_Rect position; //Contiendra la position de nos textes
	if(!jeu.estFini)//quand le jeu n'est pas fini
	{
		//SCORE DU JEU
		//Message dans une string
		std::string textetemp = "Score --> Noir: " + std::to_string(jeu.getScoreNoir())
		+ " Blanc : " + std::to_string(jeu.getScoreBlanc());
		scoreTexte = TTF_RenderText_Blended(police, textetemp.c_str(), coulnoir);
		//Definition de la position
		position.x=420;
		position.y=100;
		
		//Création d'un texture à partir du texte
		auto texturetemp=SDL_CreateTextureFromSurface(renderer,scoreTexte);
		SDL_QueryTexture(texturetemp,nullptr,nullptr,&position.w,&position.h);
		SDL_RenderCopy(renderer, texturetemp, NULL, &position);
		SDL_DestroyTexture(texturetemp);
		SDL_FreeSurface(scoreTexte);
		
		//INFO DU TOUR
		switch (jeu.getJoueurCourant())
		{
			case JOUEUR_NOIR:
				tourTexte = TTF_RenderText_Blended(police, "Au tour de Noir",coulnoir);
				break;
			case JOUEUR_BLANC:
				tourTexte = TTF_RenderText_Blended(police, "Au tour de Blanc",coulnoir);
				break;
		}
		//Definition de la position
		position.x=420;
		position.y=120;
		//Création d'un texture à partir du texte
		texturetemp=SDL_CreateTextureFromSurface(renderer,tourTexte);
		SDL_QueryTexture(texturetemp,nullptr,nullptr,&position.w,&position.h);
		SDL_RenderCopy(renderer, texturetemp, NULL, &position);
		SDL_DestroyTexture(texturetemp);
		SDL_FreeSurface(tourTexte);
	}
	else{ //QUAND LE JEU EST FINI!!
		
		//AFFICHAGE DU VAINQUEUR
		switch (jeu.getVainqueur())
		{
			case JOUEUR_NOIR:
				winTexte = TTF_RenderText_Blended(police, "NOIR VAINQUEUR !",coulnoir);
				break;
			case JOUEUR_BLANC:
				winTexte = TTF_RenderText_Blended(police, "BLANC VAINQUEUR !",coulnoir);
				break;
		}
		//definition de la position
		position.x=420;
		position.y=120;
		//Création d'un texture à partir du texte
		auto texturetemp=SDL_CreateTextureFromSurface(renderer,winTexte);
		SDL_QueryTexture(texturetemp,nullptr,nullptr,&position.w,&position.h);
		SDL_RenderCopy(renderer, texturetemp, NULL, &position);
		SDL_DestroyTexture(texturetemp);
		SDL_FreeSurface(winTexte);
		
		//AFFICHAGE SCORE FINAL
		std::string textetemp = "Score final--> Noir: " + std::to_string(jeu.getScoreNoir())
		+ " Blanc : " + std::to_string(jeu.getScoreBlanc());
		scoreTexte = TTF_RenderText_Blended(police, textetemp.c_str(), coulnoir);
		//Definition de la position
		position.x=420;
		position.y=100;
		
		//Création d'un texture à partir du texte
		texturetemp=SDL_CreateTextureFromSurface(renderer,scoreTexte);
		SDL_QueryTexture(texturetemp,nullptr,nullptr,&position.w,&position.h);
		SDL_RenderCopy(renderer, texturetemp, NULL, &position);
		SDL_DestroyTexture(texturetemp);
		SDL_FreeSurface(scoreTexte);
		
	}
	
	//On rend le résultat
	SDL_RenderPresent(renderer);
	
}

void Gui::guiQuit(){
	//basique
	TTF_CloseFont(police);
	TTF_Quit();
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();	
}

void Gui::gererClic(int x, int y){
	//D'abord on récupère les coordonnées passées en param et on les adapte
	//pour qu'elles correspondent a des indices
	auto clic = std::make_pair(x*8/400,y*8/400);
	std::cout <<std::get<0>(clic) << " , " << std::get<1>(clic) <<std::endl;
	//Si aucune action n'est possible
	if(jeu.getActions().size()==0)
	{
		//On passe
		//si on a déjà passé une fois : fin de partie
		if(jeu.dejaPasse){
			jeu.estFini=true;
			jeu.trouverGagnant();
		}//sinon rien on passe simplement
		else{
			jeu.dejaPasse=true;
			jeu.switchJoueur();
		}
		guiRender();
	}
	//Si on a des actions possibles
	//Et pour toutes ces actions possibles
	for(auto &a : jeu.getActions())
	{
		//si l'utilisateur a selectionné cette action 
		if(a==clic){
			//on la joue
			jeu.jouer(std::get<0>(clic),std::get<1>(clic));
			guiRender();
			return;
		}
	}
	std::cout << "Mouvement impossible" << std::endl;
	
}

void Gui::instance(){ // GUI LOCALE	
	//Au début on initialise notre gui
	guiInit();
	//Puis on rend tout de suite l'état initial
	guiRender();
	
	//TANT QU'ON NE QUITTE PAS LA FENETRE ON JOUE
	bool quitterFenetre = false;
	while(!quitterFenetre)
	{
		//On capture un evenement
		SDL_WaitEvent(&eventFenetre);
		switch (eventFenetre.type)
		{
			case SDL_QUIT: // Si on clique sur la croix
				quitterFenetre = true; // on quitte la fenêtre
				break;
			case SDL_MOUSEBUTTONUP: // Lors d'un clic
				switch(eventFenetre.button.button)
				{
					case SDL_BUTTON_LEFT : //Clic gauche
						if(!jeu.estFini)gererClic(eventFenetre.button.x,eventFenetre.button.y);
						break;
					case SDL_BUTTON_MIDDLE : //Clic milieu
						resetJeu();
						break;
					case SDL_BUTTON_RIGHT : // Clic droit
						guiQuit();
						return;
				}
				break;
		}
		
		
	}
	guiQuit(); // si on sort de la boucle c'est qu'on veux quitter la fenêtre donc on arrête
}

void Gui::iteration(){ //ITERATION POUR ENSCRIPTEN
	SDL_RenderPresent(renderer);

	//TANT QU'ON NE QUITTE PAS LA FENETRE ON JOUE
	bool quitterFenetre = false;
	
		//On capture un evenement
		while(SDL_PollEvent(&eventFenetre)){ // Boucle pour attendre un evenement mais non bloquant
			switch (eventFenetre.type)
			{
				case SDL_QUIT: // Si on veux quitter
					quitterFenetre = true;
					break;
				case SDL_MOUSEBUTTONUP: //clic souris
					switch(eventFenetre.button.button)
					{
						case SDL_BUTTON_LEFT : //clic gauche
							if(!jeu.estFini)gererClic(eventFenetre.button.x,eventFenetre.button.y);
							break;
						case SDL_BUTTON_MIDDLE : //clic milieu
							resetJeu();
							break;
						case SDL_BUTTON_RIGHT : //clic droit
							guiQuit();
							return;
					}
					break;
			}
		}
}


void Gui::resetJeu(){
	jeu.raz();
	guiRender();
}
