#ifndef GUI_HPP
#define GUI_HPP

#include "Reversi.hpp"
#include <functional>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>

class Gui {
	private:
	
	///\brief Contient le jeu reversi auquel on joue
	Jeu jeu;
	
	///\brief Pointeur de la fenetre SDL 
	SDL_Window * window;
	
	///\brief Pointeur du renderer SDL
	SDL_Renderer * renderer;
	
	///\brief Contient la police d'écriture de notre gui
	TTF_Font *police;
	
	///\brief Couleur de notre texte
	SDL_Color coulnoir = {0,0,0};
	
	///\brief Liste de nos pointeurs de surface de texte
	SDL_Surface *scoreTexte;
	SDL_Surface *winTexte;
	SDL_Surface *tourTexte;
	
	///\brief Evenement de notre fenetre
	SDL_Event eventFenetre;
	
	public:
	
	///\brief Constructeur basique
	Gui();
	
	///\brief Initialise la gui en créant une fenêtre, son renderer, en
	///initialisant le ttf en chargeant la police d'écriture
	/// gère l'affichage des erreurs aussi 
	void guiInit();
	
	///\brief Permet de mettre a jour l'affichage en cleanant la fenêtre
	///puis le quadrillage, l'état du plateau et enfin l'état du jeu avec le texte
	void guiRender();
	
	///\brief Détruit tout avant de fermer le programme
	void guiQuit();
	
	///\brief Permet de remettre le jeu a zero en resettant le jeu et en 
	///refaisant l'affichage de l'état initial
	void resetJeu();
	
	///\brief Gère le clic souris dans la fenêtre en fonction de ses coordonnées
	/// en appelant jouer() et en faisant avancer la partie
	void gererClic(int x , int y);
	
	///\brief Lance une instance de notre gui qui prendra en main le déroulement du jeu
	///cette instance est utilisable uniquement en local sans enscripten car elle gère la 
	///fermeture de la fenêtre avec une boucle infinie.
	void instance();
	
	///\brief Lance le jeu mais pour enscripten avec une boucle d'evenement non bloquante
	///Cette methode sera lancée sur la page html
	void iteration();
	
	
};
#endif
