#include "Reversi.hpp"

Jeu::Jeu() {
    raz();
}

/*
 * Methode reset qui permet de remettre le jeu à l'état initial
 */
void Jeu::raz() { 
	estFini=false;
	_vainqueur=JOUEUR_VIDE;
	_courant=JOUEUR_NOIR;
	score_blanc=2;
	score_noir=2;
	dejaPasse=false;
	//Met le plateau à son état initial
    for (auto & l : _plateau)
    {
		for (auto & c : l){
				c=JOUEUR_VIDE;
		}
	}
	_plateau[3][3]=JOUEUR_BLANC;
	_plateau[3][4]=JOUEUR_NOIR;
	_plateau[4][3]=JOUEUR_NOIR;
	_plateau[4][4]=JOUEUR_BLANC;
	
	//Liste les actions pour le premier joueur c a d JOUEUR_NOIR
	listerActions();
}

Plateau Jeu::getPlateau()const{
	return _plateau;
}
//OPERATEUR DE SORTIE
std::ostream & operator<<(std::ostream & os, const Jeu & jeu) {
    for (auto & l : jeu.getPlateau())
    {
		for (auto & c : l)
		{
			if(c == JOUEUR_NOIR)os<< 'B';
			else if (c == JOUEUR_BLANC)os<<'W';
			else os<<'.';
		}
		os << std::endl;
	}
    return os;
}

Joueur Jeu::getVainqueur() const {
    return _vainqueur;
}

Joueur Jeu::getJoueurCourant() const {
    return _courant;
}

int Jeu::getScoreNoir()const{
	return score_noir;
}
int Jeu::getScoreBlanc()const{
	return score_blanc;
}

/*
 * Permet de lister dans menu la liste des actions possibles
 * pour un joueur lors de son tour. A noter qu'ici on inverse l'ordre
 * d'affichage des coordonnées pour que ça correspondent à l'ordre x et y 
 * des repères orthonormés.
 * 
 * Utile pour la partie console
 */
std::string Jeu::afficherActions()const{
	std::stringstream oss;
	for (int i = 0 ; i<_actions.size() ; i++)
	{
		oss << i << "-> " << std::get<1>(_actions[i]) << " , " <<
		std::get<0>(_actions[i])<< std::endl;
	}
	std::cout << oss.str();
	return oss.str();
	
}

std::vector<Action> Jeu::getActions ()const{
	return _actions;
}

/*
 * Retourne l'état du jeu sous forme d'un menu.
 * 
 * Utile pour la partie console
 */
std::string Jeu::afficherMenu()const{
	
	std::string fini ="non"; // Etat de la partie
	if(estFini)fini="oui";
	
	std::string joueurCourant; // Joueur qui est en train de jouer
	switch (getJoueurCourant())
	{
		case 1 : //JOUEUR_BLANC
			joueurCourant="JOUEUR_BLANC";
			break;
		case 2 : //JOUEUR_NOIR
			joueurCourant="JOUEUR_NOIR";
			break;
	}
	std::stringstream oss;
	oss << "score noir: " << getScoreNoir() << std::endl;
	oss << "score blanc: " << getScoreBlanc() << std::endl;
	oss << "estFini: " << fini << std::endl;
	oss << "Tour de " << joueurCourant << std::endl;
	std::cout << oss.str();
	
	//Affiche la liste des actions
	std::cout << "ACTIONS POSSIBLES :" << std::endl;
	afficherActions();
	
	return oss.str(); //doit return une string pour pouvoir effectuer les tests
}

/*
 * Recompte les points et met à jour le score
 */
void Jeu::updateScore(){
	int blanc=0;
	int noir=0;
	
	for (auto & l : _plateau)
    {
		for (auto & c : l)
		{
			if(c == JOUEUR_NOIR)noir++;
			else if(c== JOUEUR_BLANC)blanc++;
		}
	}
	score_blanc=blanc;
	score_noir=noir;
}

/*
 * Permet d'ajouter une action trouvée pour un jeton précis à notre 
 * grand vecteur des actions possibles sans garder les doublons
 */
void Jeu::ajouterAction(std::pair<int,int> action)
{
	for (auto & l : _actions)
	{
		if(l==action)return; //si action existe deja on ne la garde pas
	}
	_actions.push_back(action);
}

/*
 * Cette méthode va parcourir tout le plateau à la recherche de jetons
 * correspondant à la couleur du joueur courant et appelera la methode*
 * chercherAction() sur chacun de ces jetons. Ensuite on va ajouter les actions
 * trouvées à notre vecteur d'actions possibles pour avoir une vision globale
 * de tous les coups possibles pour le joueur courant
 */
bool Jeu::listerActions(){
	int joueurCourant=getJoueurCourant(); // JOUEUR COURANT
	std::vector<Action> temp; // Contiendra les actions possibles à partir d'un jeton
	
	//on parcours tout notre plateau
	for (int i=0;i<_plateau.size();i++)
    {
		for (int j=0;j<_plateau[i].size();j++)
		{
			//si on tombe sur un jeton du joueur actuel
			if(_plateau[i][j] == joueurCourant)
			{
				//on cherche les coups possibles à partir de celui ci
				temp=chercherAction(std::make_pair(i,j));
				
				//ensuite on les ajoute à la liste totale des actions
				for(auto & v : temp)
				{
					ajouterAction(v);
				}
			}
		}
	}
	//si on trouve aucune action renvoi 0
	if(_actions.size()==0)
	{
		return false;
	}
	//si on a trouvé au moins 1 action renvoi 1
	return true;
}

/*
 * Cherche les actions possibles à partir d'un jeton donné (notamment ces 
 * coordonnées) 
 */
std::vector<Action> Jeu::chercherAction (std::pair<int,int> jeton){
	Joueur adversaire; // contenant l'indice de l'adversaire
	//On récupère la couleur adverse
	switch(getJoueurCourant()){
		case 2 :
			adversaire = JOUEUR_BLANC;
			break;
		case 1 :
			adversaire = JOUEUR_NOIR;
			break;
	}
	
	//On demarre a notre jeton
	int jetonCourantX=std::get<0>(jeton);
	int jetonCourantY=std::get<1>(jeton);
	
	//repond si on a vu au moins un jeton adverse
	bool auMoinsUnAdversaire=false;
	
	/*
	 * Si on est tombé sur un jeton qui fait qu'il ne peut pas y avoir de 
	 * mouvement dessus
	 * */
	bool mauvaisJeton=false; 
	
	/*
	 * Si on est tombé sur un emplacement possible pour placer le jeton
	 * */
	bool actionTrouvee=false;
	
	Joueur courant;
	
	//Liste des action possibles à partir du jeton de départ
	std::vector<Action> toreturn;
	
	//permet de suivre une ligne de jeton
	int iteration;
	
	//En regardant dans toutes les directions à partir du jeton de départ
	for (int x=-1;x<=1;x++)
	{
		for(int y=-1;y<=1;y++)
		{
			
			/*
			 * Si on tombe sur une case qui n'est pas une direction (0,0) 
			 * on passe ce cas ci
			 */
			if (x==0 && y==0)continue; 
			
			//sinon 
			iteration = 1; //on commence par regarder un jeton a 1 de distance
			//on remet les flags a false car on va commencer à regarder
			auMoinsUnAdversaire=false; 
			actionTrouvee=false;
			mauvaisJeton=false;
			
			//tant qu'on reste bien dans le plateau on regarde 
			while(jetonCourantX+x*iteration>=0 && jetonCourantX+x*iteration <=7 && jetonCourantY+y*iteration>=0 && jetonCourantY+y*iteration<=7)
				{
					//dans la direction
					courant =_plateau[jetonCourantX+x*iteration][jetonCourantY+y*iteration];
					if(courant==adversaire)//Si c'est un adversaire
					{
						iteration++; // On peut tout de suite aller voir plus loin
						auMoinsUnAdversaire=true; // et on met le flag à jour
					}
					else if(courant==JOUEUR_VIDE)//si case vide alors
					{
						if(auMoinsUnAdversaire)//Si au moins un adversaire a été vu
							{
								//C'est un emplacement valable ! On peut garder en mémoire celle ci
								toreturn.push_back(std::make_pair(jetonCourantX+x*iteration,jetonCourantY+y*iteration));
								actionTrouvee=true; // On met à jour le flag pour cette direction
							}
						else{
								//Si c'est vide et qu'on a pas encore vu d'adversaire case invalide
								mauvaisJeton=true;
							}
					}
					else 
					{
						//Si on vois un jeton de notre couleur c'est une case invalide
						mauvaisJeton=true;
					}
							
				//SI ON A TROUVE :
				if (actionTrouvee) break; //break le while donc change de direction a voir
				//SI CA MARCHE PAS :
				else if(mauvaisJeton)break; //break while donc cherche vers une autre dir
				}

		}
	}
	return toreturn; //Enfin on retourne toutes les actions à partir du jeton 
}

/*Permet de changer de joueur courant et donc de passer des tours
 * en passant un tour on liste tout de suite les actions de l'autre joueur
 */
void Jeu::switchJoueur(){
	switch(getJoueurCourant())
			{
				case 2 :
				_courant=JOUEUR_BLANC;
					break;
				case 1 :
				 _courant=JOUEUR_NOIR;
					break;
			}
	listerActions();
}

/*
 * Permet à l'utilisateur d'avoir une interface pour selectionner 
 * les actions à jouer. A noter que cette fonction sera appelé uniquement
 * si il existe des actions possibles
 * 
 * Utile pour la partie console
 */
void Jeu::selection(){
	int select;
	
	//Affichage simple
	std::cout << "Qu'elle action voulez vous jouer ?" << std::endl;
	std::cin >> select;
	
	while(select < 0 || select >= _actions.size()) // Si l'action entrée est invalide
	{
		std::cout << "Action invalide veuillez entrer une action valide :"<< std::endl;
		std::cin >> select; // on en redemande une autre valide
	}
	
	jouer(std::get<0>(_actions[select]),std::get<1>(_actions[select])); // Puis on la joue
	return;
}

/*Cette méthode permet de jouer un coup sur une case qui correspond à une
 * action possible. On va ajouter un jeton de couleur sur cette case puis 
 * retouner tout les jetons adverse encadrés par ce nouveau jeton.
 */
void Jeu::jouer(int i, int j) {
	Joueur adversaire; // contient l'indice de l'adversaire
	Joueur joueurCourant; //contient le joueur courant
	Joueur courant; //contient le jeton que l'on regarde
	//On commence par poser le jeton à l'emplacement
	switch(getJoueurCourant()){
		case 2 :
			_plateau[i][j] = JOUEUR_NOIR;
			adversaire = JOUEUR_BLANC;
			joueurCourant = JOUEUR_NOIR;
			break;
		case 1 :
			_plateau[i][j] = JOUEUR_BLANC;
			adversaire = JOUEUR_NOIR;
			joueurCourant = JOUEUR_BLANC;
			break;
	}

	//Répond si on a vu au moins un jeton adverse
	bool auMoinsUnAdversaire=false;
	
	/*
	 * Si on est tombé sur un jeton qui fait que la ligne ne doit pas être 
	 * retournée
	 * */
	bool mauvaisJeton=false; 
	
	/*
	 * Si on est tombé sur un jeton que l'on peut retourner
	 * */
	bool retournerJetons=false;
	
	//Permet de suivre une ligne
	int iteration;
	
	//A partir du jeton qu'on pose on regarde toutes les directions
    for (int x=-1;x<=1;x++)
	{
		for(int y=-1;y<=1;y++)
		{
			/*
			 * Si on tombe sur une case qui n'est pas une direction (0,0) 
			 * on passe ce cas ci
			 */
			if (x==0 && y==0)continue; 
			
			//sinon
			iteration = 1; //on commence par regarder un jeton a 1 de distance
			//on remet a false les flags car on va commencer à regarder
			auMoinsUnAdversaire=false; 
			retournerJetons=false;
			mauvaisJeton=false;
			
			//tant qu'on reste bien dans le plateau on regarde
			while(i+x*iteration>=0 && i+x*iteration <=7 && j+y*iteration>=0 && j+y*iteration<=7)
				{
					//en suivant la direction 
					courant =_plateau[i+x*iteration][j+y*iteration];
					if(courant==adversaire)//Si c'est un adversaire
					{
						iteration++; // On va tout de suite voir la suite
						auMoinsUnAdversaire=true; // on met a jour le flag
					}
					else if(courant==joueurCourant)//si on tombe sur un jeton de notre couleur
					{
						if(auMoinsUnAdversaire)//et Si au moins un adversaire a été vu
							{
								retournerJetons=true; // On va devoir retourner les jetons
							}
						else{ //si on a pas vu d'adversaire ça casse l'encadrement
								mauvaisJeton=true;
							}
					}
					else //si ya vide ou autre pas d'encadrement
					{
						mauvaisJeton=true;
					}
							
					/*Si on a trouvé un jeton qui encadre un ou plusieurs jetons
					* adverses : on retourne tout les jetons adverses
					* */
					if (retournerJetons)
						{
							for (int iter = 1 ; iter<iteration; iter++){
							_plateau[i+x*iter][j+y*iter]=joueurCourant;
							}
							
							/*si on a retourné les jetons dans une direction on peut aller
							*chercher vers une autre direction
							* */
							break;
						}
					//si il n'y a rien dans cette direction :
					else if(mauvaisJeton)break; //break while donc cherche vers une autre dir
				}
		}
	}
	
	
	//Après avoir joué 
	dejaPasse=false; // on remet ce flag a false car on a pu jouer
	_actions.clear(); // on clear la liste des actions possibles
	updateScore(); // on met à jour le score
	switchJoueur(); // et on change de joueur
    return;
}

/*Lors de l'appel cette foncion va déterminer le gagant. A noter
 * que cette methode doit être appelée à la fin de la partie 
 */
void Jeu::trouverGagnant(){
	if(score_blanc>score_noir)
	{
		_vainqueur=JOUEUR_BLANC;
	}
	else if(score_noir>score_blanc){
		_vainqueur=JOUEUR_NOIR;
	}
	else{
		_vainqueur=JOUEUR_EGALITE;
	}
}
