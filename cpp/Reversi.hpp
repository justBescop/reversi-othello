#ifndef REVERSI_HPP
#define REVERSI_HPP

#include <array>
#include <iostream>
#include <string>
#include <sstream>
#include <utility>
#include <vector>

///\brief Notre enum des valeurs possibles pour chaque case
enum Joueur { JOUEUR_VIDE, JOUEUR_BLANC, JOUEUR_NOIR, JOUEUR_EGALITE };

///\brief Le plateau est un tableau 2 dimensions de 8 sur 8 contenant des 
///joueurs représentant chacune des cases.
using Plateau = std::array<std::array<Joueur, 8>, 8>;

///\brief Une action est une pair qui contient les coordonnées d'une action
///possible sur le plateau en x et y
using Action = std::pair<int,int>;

class Jeu {
    private:
        ///\brief Le plateau de jeu
        Plateau _plateau;      
        
        ///\brief Joueur courant qui est en train de jouer
        Joueur _courant;
        
        ///\brief Joueur qui a gagné
        /// doit être à joueur vide si la partie n'est pas finie
        /// egalite si il y a egalite et couleur du vainqueur sinon
        Joueur _vainqueur;
		
		///\brief Contiendra le score du joueur noir
		int score_noir;
		///\brief Contiendra le score du joueur blanc
		int score_blanc;
		
		
    public:
		///\brief Booléen qui indique si la partie est finie ou non
		bool estFini;
		
		///\brief Booléen qui indique si le joueur précédent ne pouvait pas jouer et a donc passé
		///son tour
		bool dejaPasse;
		
		///\brief Donne la liste des actions possibles pour le joueur
        ///courant à chaque tour (mit en public pour les tests unitaires)
        std::vector<Action> _actions;  
        
        ///\brief Constructeur trivial
        ///doit initialiser le vainqueur a vide
        ///et le joueur noir commence la partie
        Jeu();
		
		///\brief Getteur du plateau
		///\return le plateau de jeu
		Plateau getPlateau()const;
		
		
        ///\brief Getteur du vainqueur
        ///\return le vainqueur du jeu
        Joueur getVainqueur() const;

        ///\brief Getteur du Joueur courant
        ///\return le joueur courant qui doit jouer
        Joueur getJoueurCourant() const;
		
		///\brief Getteur du score du joueur noir
		///\return score joueur noir
		int getScoreNoir()const;
		
		///\brief Getteur du score du joueur blanc
		///\return score joueur blanc
		int getScoreBlanc()const;
		
				
		///\brief Affiche le choix des actions
		///\return la string de l'affichage
		std::string afficherActions()const;
		
		///\brief Affiche le résumé de l'état du jeu
		/// On y verra le joueur courant, les scores des joueurs ainsi 
		/// que l'état de la partie (finie ou non)
		///On aura aussi un appel à la liste des actions possibles pour apporter
		///à l'utilisateur un visuel de toutes les actions possibles
		///\return la string de l'affichage
		std::string afficherMenu()const;

		///\brief Recompte les points et met à jour les scores des joueurs
		///
		void updateScore();
		
		///\brief Va ajouter une action trouvée à notre tableau
		///sauf si cette possibilité existe déjà
		void ajouterAction(std::pair<int,int> action);
		
		///\brief Permet de chercher toutes les actions possibles à
		///partir d'un jeton en particulier
		///\return vector d'action du jeton
		std::vector<Action> chercherAction (std::pair<int,int> jeton);
		
		///\brief Permet de trouver toutes les actions possibles
		///\return si au moins une action est possible
		bool listerActions();
		
		///\brief Getteur des actions possibles
		///\return le vecteur contenant les actions possibles
		std::vector<Action> getActions ()const;
		
		///\brief Permet de selectionner une action puis de jouer
		///en fonction des actions possibles qui ont été listée avant
		void selection ();
		
		///\brief Permet de changer de joueur et de passer la main
		///on listera les actions du prochain joueur en même temps
		void switchJoueur();
			
        ///\brief Permet de jouer un jeton à l'endroit indiqué
        /// et retourne tous les jetons qui doivent l'être
        /// De plus on oublie les actions possibles et on peut 
        ///changer de joueur
        void jouer(int i, int j);
		
		///\brief Definit le gagnant
		void trouverGagnant();
		
        ///\brief Réinitialise le jeu.
        void raz();
		
        friend std::ostream & operator<<(std::ostream & os, const Jeu & jeu);
};

std::ostream & operator<<(std::ostream & os, const Jeu & jeu);

#endif

