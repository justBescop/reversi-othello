#include "Reversi.hpp"
#include <sstream>

#include <CppUTest/CommandLineTestRunner.h>

void testerJeu(const std::string & str, const Jeu & jeu) {
    std::stringstream oss;
    oss << jeu;
    CHECK_EQUAL(str, oss.str());
}

TEST_GROUP(GroupReversi) { };

TEST(GroupReversi, test_Jeu) { 
    Jeu jeu;
    CHECK(true);
}
TEST(GroupReversi, test_affichage1){
	Jeu jeu;
	testerJeu("........\n........\n........\n...WB...\n...BW...\n........\n........\n........\n",jeu);
}

TEST(GroupReversi, test_initVainqueur) { 
    Jeu jeu;
    CHECK_EQUAL(jeu.getVainqueur(),JOUEUR_VIDE);
}

TEST(GroupReversi, test_initCourant) { 
    Jeu jeu;
    CHECK_EQUAL(jeu.getJoueurCourant(),JOUEUR_NOIR);
}

TEST(GroupReversi, test_afficherMenu) { 
    Jeu jeu;
    std::string menu = jeu.afficherMenu();	
    std::string test = "score noir: 2\nscore blanc: 2\nestFini: non\nTour de JOUEUR_NOIR\n";
    CHECK_EQUAL(test,menu);
}
TEST(GroupReversi, test_chercherCoup1) { 
    Jeu jeu;
    
    int act11 = 3;
    int act12 = 2;
    int act21 = 5;
    int act22 = 4;
    
    std::vector<Action> temp = jeu.chercherAction(std::make_pair(3,4)); 
    
    CHECK_EQUAL(act11,std::get<0>(temp[0]));
    CHECK_EQUAL(act12,std::get<1>(temp[0]));
	CHECK_EQUAL(act21,std::get<0>(temp[1]));
	CHECK_EQUAL(act22,std::get<1>(temp[1]));
}
TEST(GroupReversi, test_listerAction) { 
	int act11 = 3;
    int act12 = 2;
    int act21 = 5;
    int act22 = 4;
    int act31 = 2;
    int act32 = 3;
    int act41 = 4;
    int act42 = 5;
    
    Jeu jeu;
    jeu.listerActions();	
    CHECK_EQUAL(act11,std::get<0>(jeu._actions[0]));
    CHECK_EQUAL(act12,std::get<1>(jeu._actions[0]));
	CHECK_EQUAL(act21,std::get<0>(jeu._actions[1]));
	CHECK_EQUAL(act22,std::get<1>(jeu._actions[1]));
	CHECK_EQUAL(act31,std::get<0>(jeu._actions[2]));
	CHECK_EQUAL(act32,std::get<1>(jeu._actions[2]));
	CHECK_EQUAL(act41,std::get<0>(jeu._actions[3]));
	CHECK_EQUAL(act42,std::get<1>(jeu._actions[3]));
}

TEST(GroupReversi, test_afficherAction) { 
    Jeu jeu;
    jeu.listerActions();
    std::string actions = jeu.afficherActions();	
    std::string test = "0-> 2 , 3\n1-> 4 , 5\n2-> 3 , 2\n3-> 5 , 4\n";
    CHECK_EQUAL(test,actions);
}
TEST(GroupReversi, test_Jouer1){
	Jeu jeu;
	jeu.listerActions();
	jeu.jouer(std::get<0>(jeu._actions[0]),std::get<1>(jeu._actions[0]));
	testerJeu("........\n........\n........\n..BBB...\n...BW...\n........\n........\n........\n",jeu);
}
TEST(GroupReversi, test_Jouer2){
	Jeu jeu;
	jeu.listerActions();
	jeu.jouer(std::get<0>(jeu._actions[0]),std::get<1>(jeu._actions[0]));
	jeu.listerActions();
	jeu.jouer(std::get<0>(jeu._actions[2]),std::get<1>(jeu._actions[2]));
	testerJeu("........\n........\n........\n..BBB...\n..WWW...\n........\n........\n........\n",jeu);
}
