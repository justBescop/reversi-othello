{ pkgs ? import <nixpkgs> {} }:

with pkgs;

stdenv.mkDerivation {
  name = "reversi";
  src = ./.;
  buildInputs = [
    cmake
    pkgconfig
    doxygen
    cpputest
  ];
}

