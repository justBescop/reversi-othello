#include "Reversi.hpp"

using namespace std;

int main() {
	Jeu jeu;
	
	//DEROULEMENT DE LA PARTIE SUR CONSOLE !
	
	//tant que la partie n'est pas finie
	while (!jeu.estFini){
		std::cout<<jeu<<std::endl; // on affiche le plateau
		if(jeu.getActions().size()!=0) //et si au moins une action est possible
		{
			// on affiche l'interface
			jeu.afficherMenu(); 
			jeu.selection();
			jeu.dejaPasse=false;
		}
		//si aucune action est possible on passe
		else if(jeu.dejaPasse) // mais si on a deja passé
		{
			//Fin de partie
			jeu.estFini=true; 
			jeu.trouverGagnant();
		}
		else // si on a pas encore passé
		{
			jeu.dejaPasse=true; //on le fait
			switch(jeu.getJoueurCourant()){
				case 1 :
					std::cout<<"JOUEUR_BLANC passe" <<std::endl;
					break;
				case 2 :
					std::cout<<"JOUEUR_NOIR passe" <<std::endl;
					break;
			}
			jeu.switchJoueur();
			
		}
	}
	
	
	//A la fin on sort de la boucle et on affiche le résultat final
	switch (jeu.getVainqueur())
	{
		case 1:
			std::cout << "Le gagnant est JOUEUR_BLANC"<<std::endl;
			break;
		case 2:
			std::cout << "Le gagnant est JOUEUR_NOIR"<<std::endl;
			break;
		case 3:
			std::cout << "EGALITE !"<<std::endl;
			break;
	}
	std:: cout << "SCORE :" << jeu.getScoreNoir()<<" / " << jeu.getScoreBlanc() << std::endl;
		
    return 0;
}

