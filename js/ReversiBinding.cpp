#include "../cpp/Reversi.hpp"
#include "../cpp/Gui.hpp"
#include <emscripten/bind.h>
#include <emscripten.h>
#include <emscripten/html5.h>

Gui gui;

void boucle(){
		gui.iteration();
}

int main(){
	gui.guiInit(); // on init notre gui
	gui.guiRender(); // puis on l'affiche 
	emscripten_set_main_loop(boucle, 0,1); // et enfin on lance la boucle du jeu
	return 0;
}

